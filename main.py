import os
import sys
import json
from libs.driveApi import DriveApi, InvalidCredentials
from libs.sync import DriveSync
from libs.imageResizer import ImageResizeService

try:
    with open('./settings.json', 'r') as fh:
        settings = json.load(fh)
except FileNotFoundError:
    FileNotFoundError('you must have a settings.json')

DRIVE_TARGET_FOLDER = settings.get('driveTargetFolder')
DRIVE_OUTPUT_FOLDER = settings.get('driveOutputFolder')
IMAGE_RESIZER_INPUT_FOLDER = settings.get('imageResizerInputFolder')
IMAGE_RESIZER_OUTPUT_FOLDER = settings.get('imageResizerOutputFolder')
IMAGE_RESIZER_ERROR_FOLDER = settings.get('imageResizerErrorFolder')
DEBUG = settings.get('debug')


def clean_folder(folder):
    files = os.listdir(folder)
    for file in files:
        if file == ".keep":
            continue
        os.remove(f'{folder}/{file}')


def firstTimeExecution():
    drive = DriveApi(credentials_path='./vault')
    if not drive.valid_creds():
        raise InvalidCredentials('Invalid Credentials')
        sys.exit()

    service = drive.build_service()

    sync = DriveSync(
        drive_service=service,
        target_folder=DRIVE_TARGET_FOLDER,
        output_path=DRIVE_OUTPUT_FOLDER
    )

    sync.empty_trash()
    sync.download_files_in_folder(
        delete_from_cloud=True if not DEBUG else True
    )

    image_service = ImageResizeService(
        spath=IMAGE_RESIZER_INPUT_FOLDER,
        epath=IMAGE_RESIZER_OUTPUT_FOLDER,
        errpath=IMAGE_RESIZER_ERROR_FOLDER,
        size=(443, 443), q=100, format_name='%(name)s'
    )
    image_service.resize_images(extensions=('jpeg', 'png', 'jpg'))
    clean_folder(DRIVE_OUTPUT_FOLDER)


def trackChanges():
    drive = DriveApi(credentials_path='./vault')
    if drive.valid_creds():
        service = drive.build_service()

    sync = DriveSync(
        drive_service=service,
        target_folder=DRIVE_TARGET_FOLDER,
        output_path=DRIVE_OUTPUT_FOLDER
    )
    sync.empty_trash()

    hasChanges = sync.retrive_changes(
        delete_from_cloud=True if not DEBUG else False
    )

    if hasChanges:
        image_service = ImageResizeService(
            spath=IMAGE_RESIZER_INPUT_FOLDER,
            epath=IMAGE_RESIZER_OUTPUT_FOLDER,
            errpath=IMAGE_RESIZER_ERROR_FOLDER,
            size=(443, 443), q=100, format_name='%(name)s'
        )
        image_service.resize_images(extensions=('jpeg', 'png', 'jpg'))

        clean_folder(DRIVE_OUTPUT_FOLDER)
