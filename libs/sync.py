from .driveApi import DriveApi
import io
import os
import re
import json
import logging
from googleapiclient.http import MediaIoBaseDownload
from PIL import Image


logger = logging.getLogger(__name__)
excluded_logger = logging.getLogger(__name__ + 'excluded')
new_files_logger = logging.getLogger("new_images")
deleted_files_logger = logging.getLogger("deleted_files")

handler = logging.FileHandler('./logs/sync.log')
excluded_handler = logging.FileHandler('./logs/sync_excluded_files.log')
new_files_handler = logging.FileHandler('./logs/new_files.log', mode="w")
deleted_files_handler = logging.FileHandler('./logs/deleted_files.log')


handler.setFormatter(logging.Formatter(
    '%(asctime)s:%(levelname)s -> %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p'
))
excluded_handler.setFormatter(logging.Formatter(
    '%(asctime)s:%(levelname)s -> %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p'
))
deleted_files_handler.setFormatter(logging.Formatter(
    '%(asctime)s:%(levelname)s -> %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p'
))
new_files_handler.setFormatter(logging.Formatter(
    '%(message)s'
))

logger.addHandler(handler)
logger.setLevel(logging.INFO)

excluded_logger.addHandler(excluded_handler)
excluded_logger.setLevel(logging.INFO)

deleted_files_logger.addHandler(deleted_files_handler)
deleted_files_logger.setLevel(logging.INFO)

new_files_logger.addHandler(new_files_handler)
new_files_logger.setLevel(logging.INFO)


DIR = os.path.dirname(os.path.abspath(__file__))


class DriveSync(object):

    PAGES_TOKEN = f'{DIR}/pages_tokens.json'

    def __init__(self, drive_service, target_folder, output_path):
        self.target_folder = target_folder
        self.output_path = output_path
        self.drive_service = drive_service
        self.tokens = self._load_pages_tokens()

    def _load_pages_tokens(self):
        try:
            with open(self.PAGES_TOKEN, 'r') as json_file:
                tokens = json.load(json_file)
        except:
            tokens = {
                'start_page_token': self.first_time_start_page_token(),
                'last_page_token': None
            }

        return tokens

    def _dump_pages_tokens(self):
        tokens = {
            'start_page_token': self.tokens.get('start_page_token'),
            'last_page_token': self.tokens.get('last_page_token')
        }

        with open(self.PAGES_TOKEN, 'w') as json_file:
            json.dump(tokens, json_file)

    def first_time_start_page_token(self):
        response = self.drive_service.changes().getStartPageToken().execute()
        return response.get('startPageToken')

    def retrive_changes(self, delete_from_cloud=False):
        page_token = self.tokens.get('last_page_token')
        if not page_token:
            page_token = self.tokens.get('start_page_token')

        folder_filter = f'name="{self.target_folder}" and \
            mimeType="application/vnd.google-apps.folder" and trashed=false'

        folder_id = self.drive_service.files().list(
            q=folder_filter
        ).execute().get('files')[0].get('id')

        partial_query = '''
            nextPageToken,
            newStartPageToken,
            changes(
                type, removed,
                file(
                    id, name, mimeType, trashed, parents
                )
            )
        '''.replace('\n', '').replace('    ', '')

        while page_token is not None:
            response = self.drive_service.changes().list(
                pageToken=page_token, spaces='drive', fields=partial_query
            ).execute()
            changes = response.get('changes')
            if not changes:
                logger.info(f'Nothing changed in folder {self.target_folder}')
                return False

            for change in changes:
                file = change.get('file', None)
                if not file:
                    continue
                is_target = folder_id in file.get('parents', [])
                if is_target:
                    if change['removed'] or file['trashed']:
                        logger.warning(f'file {file["name"]} was removed from source folder')
                    else:
                        self.download_file(
                            file_id=file['id'], file_name=file['name'],
                            delete_from_cloud=delete_from_cloud
                        )
                        logger.info(f'file {file["name"]} was download')

            if 'newStartPageToken' in response:
                self.tokens['last_page_token'] = response.get(
                    'newStartPageToken'
                )

            page_token = response.get('nextPageToken')

        self._dump_pages_tokens()
        return True

    def download_files_in_folder(self, delete_from_cloud=False):
        folder_filter = f'name="{self.target_folder}" and \
            mimeType="application/vnd.google-apps.folder" and trashed=false'

        folder_id = self.drive_service.files().list(
            q=folder_filter
        ).execute().get('files')[0].get('id')
        response = self.drive_service.files() \
            .list(q=f'"{folder_id}" in parents') \
            .execute()

        files = response.get('files')
        for file in files:
            self.download_file(file['id'], file['name'])
            logger.info(f'file {file["name"]} was download')

        firstNextPageToken = response.get('nextPageToken')
        pageToken = firstNextPageToken
        while pageToken:
            response = self.drive_service.files() \
                .list(pageToken=pageToken, q=f'"{folder_id}" in parents') \
                .execute()

            nextPageToken = response.get('nextPageToken')
            if nextPageToken == firstNextPageToken:
                break

            files = response.get('files')
            for file in files:
                self.download_file(
                    file['id'], file['name'], delete_from_cloud=delete_from_cloud
                )
                logger.info(f'file {file["name"]} was download')

            pageToken = nextPageToken

    def download_file(self, file_id, file_name, delete_from_cloud=False):
        request = self.drive_service.files().get_media(fileId=file_id)

        output_path = self.output_path
        reg = re.compile(r'(?:\(\d+\)|copia)')
        if reg.search(file_name):
            output_path = './excluded_files'
            excluded_logger.warning(
                f'file {file_name} need to be revised, it will be exclude'
            )
        else:
            _name = file_name.split(".")[0]  # remove extension
            new_files_logger.info(f'{_name}')

        fh = io.FileIO(f"{output_path}/{file_name}", 'wb')
        done = False
        downloader = MediaIoBaseDownload(fh, request)
        while not done:
            status, done = downloader.next_chunk()

        print(f'file {file_name} was download')
        if delete_from_cloud:
            self.delete_file(file_id, extra_file_name=file_name)

    def delete_file(self, file_id, extra_file_name=""):
        try:
            self.drive_service.files().delete(fileId=file_id).execute()
            deleted_files_logger.info(f'file {extra_file_name} with ID: {file_id} was deleted')
        except Exception as err:
            deleted_files_logger.error(str(err) + f" -> {extra_file_name}")

    def empty_trash(self):
        self.drive_service.files().emptyTrash().execute()
        deleted_files_logger.info(f"The bin was emptied")





