import os
import io
import struct
import shutil
import codecs
import logging

from PIL import Image


logger = logging.getLogger(__name__)
optimized_logger = logging.getLogger('optimized_images')

handler = logging.FileHandler('./logs/resizer.log')
optimized_handler = logging.FileHandler('./logs/optimizedNewFiles.log', mode='w')

handler.setFormatter(logging.Formatter(
    '%(asctime)s:%(levelname)s -> %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p'
))

optimized_handler.setFormatter(logging.Formatter(
    '%(message)s'
))

logger.addHandler(handler)
logger.setLevel(logging.INFO)


optimized_logger.addHandler(optimized_handler)
optimized_logger.setLevel(logging.INFO)


def pure_pil_alpha_to_color_v1(image, color=(255, 255, 255, 0)):
    """Alpha composite an RGBA Image with a specified color.

    NOTE: This version is much slower than the
    alpha_composite_with_color solution. Use it only if
    numpy is not available.

    Source: http://stackoverflow.com/a/9168169/284318

    Keyword Arguments:
    image -- PIL RGBA Image object
    color -- Tuple r, g, b (default 255, 255, 255)

    """
    def blend_value(back, front, a):
        return (front * a + back * (255 - a)) // 255

    def blend_rgba(back, front):
        result = [blend_value(back[i], front[i], front[3]) for i in (0, 1, 2)]
        return tuple(result + [255])

    im = image.copy()  # don't edit the reference directly
    p = im.load()  # load pixel array
    for y in range(im.size[1]):
        for x in range(im.size[0]):
            p[x, y] = blend_rgba(color + (255,), p[x, y])

    return im

def pure_pil_alpha_to_color_v2(image, color=(255, 255, 255)):
    """Alpha composite an RGBA Image with a specified color.

    Simpler, faster version than the solutions above.

    Source: http://stackoverflow.com/a/9459208/284318

    Keyword Arguments:
    image -- PIL RGBA Image object
    color -- Tuple r, g, b (default 255, 255, 255)

    """
    image.load()  # needed for split()
    background = Image.new('RGB', image.size, color)
    background.paste(image, mask=image.split()[3])  # 3 is the alpha channel
    return background


class ImageResizeService(object):

    def __init__(self, spath=None, epath=None, errpath=None, size=(None, None), q=100, after_resize=None, format_name=None):
        self.spath = spath
        self.epath = epath
        self.errpath = errpath
        self.size = size
        self._ws, self._hs = self.size
        self.quality = q
        self.bgcolor = 'FFFFFF'
        self.trasparency = False
        self.output_format = 'jpg'
        self.after_resize = after_resize
        self.forced = False
        self.format_name = format_name

    @property
    def _output_extension_format(self):
        return 'JPEG' if self.output_format.upper() == 'JPG' else self.output_format.upper()

    @property
    def width(self):
        return self._ws

    @property
    def height(self):
        return self._hs

    def _listdir(self, ext):
        for file in os.listdir(self.spath):
            if file.endswith(".%s" % ext):
                file = "%s/%s" % (self.spath, file)
                yield os.path.abspath(file)

    def push_update(self, code):
        try:
            import requests
            from requests.auth import HTTPBasicAuth
            auth = HTTPBasicAuth('test', 'test')
            api_url = 'http://localhost:8080'
            params = {}
            refs = code.split('_')
            last_value = refs[-1].upper()
            only_version = last_value[1:]
            has_version = only_version.isdigit()
            if has_version:
                code = '_'.join(refs[:-1])
            else:
                code = '_'.join(refs)
            req = requests.post(api_url + '/api/producthistory/' + code, params=params, auth=auth, timeout=3 * 60)
            return req.ok
        except Exception as e:
            return True

    def resize_images(self, extensions=('jpg',), limit=0, skip_errors=True):
        if not self.spath or not os.path.exists(self.spath):
            raise ValueError('La ruta de inicio no existe.')
        if not self.epath or not os.path.exists(self.epath):
            raise ValueError('La ruta de fin no existe.')

        ws = self.width
        hs = self.height
        q = self.quality

        for extension in extensions:
            for ext in [extension.lower(), extension.upper()]:
                for index, file_path in enumerate(self._listdir(ext)):
                    if limit > 0 and index >= limit:
                        print("Limite alcanzado.")
                        return
                    try:
                        name, img = self.get_resized_image(file_path, ws, hs, q, self.bgcolor, self.trasparency)
                        final_path = '%s/%s.%s' % (self.epath, name, self.output_format)
                        if self.forced:
                            if os.path.exists(final_path):
                                os.remove(final_path)
                        img.save(final_path, format=self._output_extension_format, quality=self.quality)
                        try:
                            fp = {'final_path': final_path}
                            os.system('jpegtran -optimize -progressive -outfile "%(final_path)s" "%(final_path)s"' % fp)
                            os.system('jpegoptim -m 90 --strip-all "%(final_path)s"' % fp)
                        except Exception as e:
                            logger.error(f"No se pudo optimizar, desc: {e}")
                        # catnbr = os.path.basename(file_path)
                        # catnbr = ''.join(catnbr.split('.')[:-1])
                        # self.push_update(catnbr)
                        optimized_logger.info(f'optimized/{name}')
                        print('Imagen salvada %s' % name)

                        if self.after_resize and callable(self.after_resize):
                            self.after_resize(self, file_path)
                    except Exception as e:
                        if not skip_errors:
                            raise e

                        if self.errpath:
                            if not os.path.exists(self.errpath):
                                os.mkdir(self.errpath)
                            image_name = file_path.split('/')[-1]
                            shutil.copy(file_path, f'{self.errpath}/{image_name}')

                        logger.error(f"Error con el archivo: {name}, desc: {e}")
                        optimized_logger.error(f'non-optimized/{name}')

    def get_resized_image(self, base_image, ws, hs, quality=100, bgcolor="FFFFFF", trasparency=None, format_name='%(name)s_%(size_w)sx%(size_h)s_%(quality)s'):
        format_name = self.format_name or format_name
        if isinstance(base_image, (str)):
            if os.path.exists(base_image):
                base_image = Image.open(base_image)

        if isinstance(base_image, io.FileIO):
            base_image = Image.open(base_image)

        trasparency = self.trasparency or False if trasparency is None else self.trasparency or False
        size = (ws, hs)

        name = os.path.basename(os.path.splitext(base_image.filename)[0])

        image = base_image

        def trim(im):
            from PIL import Image, ImageChops
            bg = Image.new(im.mode, im.size, im.getpixel((0, 0)))
            diff = ImageChops.difference(im, bg)
            diff = ImageChops.add(diff, diff, 2.0, 0)
            bbox = diff.getbbox()
            if bbox:
                return im.crop(bbox)

        if image.mode == 'RGBA' and self.output_format != 'png':
            image_type = 'RGB'
        else:
            image_type = 'RGBA'

        image = trim(image)
        if image_type == 'RGB':
            image = pure_pil_alpha_to_color_v2(image)

        newsize = max(image.size) + 20
        newsize = (newsize, newsize)

        baseimg = Image.new(image_type, newsize, (255, 255, 255, 255))
        baseimg.paste(image, ((newsize[0] - image.size[0]) // 2, (newsize[1] - image.size[1]) // 2))
        image = baseimg
        if size[0] // 2 > image.size[0] and size[1] // 2 > image.size[1]:
            pass
        else:
            image = image.resize(size, Image.ANTIALIAS)
            image = trim(image)

        image.convert(image_type)

        decoded_bgcolor = codecs.decode(bgcolor, 'hex')
        background_color = struct.unpack('BBB', decoded_bgcolor)

        if image_type == 'RGBA':
            if trasparency:
                background_color = (255, 255, 255, 0)
            else:
                background_color += (255,)
        background = Image.new(image_type, size, background_color)
        background.paste(image, ((size[0] - image.size[0]) // 2, (size[1] - image.size[1]) // 2))
        context_name = {
            'name': name,
            'size_w': size[0],
            'size_h': size[1],
            'quality': quality,
        }
        name = format_name % context_name
        return name, background.convert('RGB')
