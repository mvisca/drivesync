from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools


class InvalidCredentials(Exception):
    pass


class DriveApi(object):
    SCOPES = 'https://www.googleapis.com/auth/drive'
    API_NAME = 'drive'
    API_VERSION = 'v3'

    def __init__(self, credentials_path=None):

        self.CREDENTIALS_JSON = f'{credentials_path}/credentials.json'
        self.TOKEN = f'{credentials_path}/token.json'
        self.creds = self._load_creds()
        
    def _load_creds(self):
        if hasattr(self, 'TOKEN'):
            store = file.Storage(self.TOKEN)
            creds = store.get()
            return creds
        
        raise AttributeError('TOKEN attr not assigned')

    def valid_creds(self):
        if not self.creds or self.creds.invalid:
            return False

        return True

    def authorize(self, non_local=False):
        flow = client.flow_from_clientsecrets(
            self.CREDENTIALS_JSON, self.SCOPES
        )
        store = file.Storage(self.TOKEN)
        if non_local:
            args = tools.argparser.parse_args()
            args.noauth_local_webserver = True
            self.creds = tools.run_flow(flow, store, args)
            return
        
        self.creds = tools.run_flow(flow, store)
    
    def build_service(self):
        if self.valid_creds():
            return build(
                self.API_NAME, self.API_VERSION,
                http=self.creds.authorize(Http())
            )
        
        raise InvalidCredentials('The credentials are not valid')
